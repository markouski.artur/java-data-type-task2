import java.io.FilterOutputStream;

public class Task2Ex3 {
    public static void main(String[] args) {
        System.out.println(ex3(4,5,4));
    }
    public static double ex3(int a, int b, int c){
        double ac4 = 4 * a * c;
        double bpom = Math.pow(b ,2 );
        double sq = Math.sqrt(bpom + ac4);
        double a3 = Math.pow(a,3);
        double b2 = Math.pow(b, -2);
        return ((b + sq) / 2 * a) - (a3 * c) + b2;
    }
}
