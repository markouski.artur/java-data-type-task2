public class Task2Ex4 {
    public static void main(String[] args) {
        System.out.println("Calculate the perimeter and area of a right triangle by the lengths a and b of the two legs.");
        perimeterAndArea(3,5);
    }
    public static void perimeterAndArea(int a, int b){
        int perimeter = 2 *(a + b);
        System.out.println("Perimeter = " + perimeter);
        int area = a * b;
        System.out.println("Area = " + area);
    }
}
