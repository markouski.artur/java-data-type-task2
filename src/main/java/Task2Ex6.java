public class Task2Ex6 {
    public static void main(String[] args) {
        System.out.println("Calculate the circumference and area of a circle of the same given radius R.");
        circumferenceAndArea(4);
    }
    public static void circumferenceAndArea(int r){
        double circumference = (3.14 * r) * 2;
        System.out.println("circumference = " + circumference);
        double area = 3.14 * Math.pow(r,2);
        System.out.println("area = " + area);
    }
}
