public class Task2Ex2 {
    public static void main(String[] args) {
        System.out.println("Ex2.Find the value of the function z = ((a - 3) * b/2) + c ");
        System.out.println(ex2(2,5,7));
    }
    public static double ex2(int a, int b, int c){
        int first = a - 3;
        double second =(double) b / 2;
        return (first * second) + c;
    }
}
