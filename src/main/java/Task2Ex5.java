public class Task2Ex5 {
    public static void main(String[] args) {
        System.out.println("Calculate the distance between two points with the" +
                " given coordinates (x1,y1) and (x2,y2) ");
        System.out.println(distance(1,3,4,5));
    }
    private static double distance(int x1, int x2, int y1, int y2){
        return Math.sqrt((Math.pow((x2 -x1),2) + Math.pow((y2 -y1),2)));
    }

}
