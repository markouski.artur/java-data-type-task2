public class Task2Ex1 {
    public static void main(String[] args) {
        System.out.println("Given two real numbers x and y. Calculate their sum, difference, product and quotient.");
        sum(4,5);
        difference(8,5);
        product(4,4);
        quotient(8,4);
    }
    public static void sum(int a, int b){
        int sum = a + b;
        System.out.println("sum = " + sum);
    }
    public static void difference(int a, int b){
        int difference = a - b;
        System.out.println("difference = " + difference);
    }
    public static void product(int a, int b){
        int product = a * b;
        System.out.println("product = " + product);
    }
    public static void quotient(int a, int b){
        double quotient =(double) a / b;
        System.out.println("quotient = " + quotient);
    }
}
