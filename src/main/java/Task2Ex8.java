public class Task2Ex8 {
    public static void main(String[] args) {
        product(222);
    }
    public static void product(int num){
        int prod = 1;
        String s = String.valueOf(num);
        char [] chars = s.toCharArray();
        for (int i = 0; i <= chars.length -1; i ++){
            prod *= Character.getNumericValue(chars[i]);
        }
        System.out.println(prod);
    }
}
